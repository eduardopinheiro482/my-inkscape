\documentclass[titlepage,a4paper, 12pt]{article}
% tamanho da página e margens
\usepackage[a4paper]{geometry}
\geometry{
  % distância entre o início da página e o início do texto principal
  top=32mm,
  bottom=28mm,
  left=24mm,
  right=34mm,
  % Com geometry, esta medida não é tão relevante; basta garantir que ela
  % seja menor que "top" e que o texto do cabeçalho caiba nela.
  headheight=25.4mm,
  % distância entre o início do texto principal e a base do cabeçalho;
  % ou seja, o cabeçalho "invade" a margem superior nessa medida. Essa
  % é a medida que determina a posição do cabeçalho
  headsep=11mm,
  footskip=10mm,
  marginpar=20mm,
  marginparsep=5mm,
}

% Vários comandos auxiliares para o desenvolvimento de packages e classes;
% aqui, usamos em alguns comandos de formatação e condicionais.
\usepackage{etoolbox}
\usepackage{xstring}
\usepackage{xparse}

\input{extras/basics}
\input{extras/fonts}
\input{extras/floats}
\input{extras/utils}

% Diretório onde estão as figuras; com isso, não é preciso colocar o caminho
% completo em \includegraphics (e nem a extensão).
\graphicspath{{figures/}}

% Comandos rápidos para mudar de língua:
% \en -> muda para o inglês
% \br -> muda para o português
% \texten{blah} -> o texto "blah" é em inglês
% \textbr{blah} -> o texto "blah" é em português
\babeltags{br = brazil, en = english}

\title {MAC0210 -- Laboratório de Métodos Numéricos \\ EP1}
\date{08-12-2018}

\author{Eduardo Pinheiro (8936798)}
\begin{document}
\maketitle

\section{Problema}

\begin{itemize}
\item Baixe o \emph{inkscape} em \url{https://inkscape.org} e aprenda como as ferramentas básicas
de edição de curvas de \emph{Bézier} funcionam. Procure desenhos (arquivo .svg)
bacana na \emph{web} para ver o que é possível fazer com tais curvas.

\item Descubra como fazer um aplicativo simplezinho, na linguagem/\emph{framework} que
você preferir, no qual você consegue desenhar linhas retas em uma janela e tratar
eventos de \emph{mouse}. Aprenda como lidar com tais eventos.

\item Usando os primitivos para traçar linhas retas, e o algoritmo de \emph{De Casteljau},
desenhe algumas curvas de \emph{Beziér} de grau dois e três na tela do seu aplicativo.

\item O seu aplicativo deve permitir que o usuário escolha, dentre as curvas desenhadas,
aquela que estiver mais próxima de onde ele \emph{clicar}. As curvas devem ser
pintadas em {\color{red}vermelho}, e quando o usuário \emph{clicar} na tela a curva mais próxima
deverá aparecer em {\color{green}verde}.
\end{itemize}

\section{Desenvolvimento}
O \emph{EP} foi desenvolvido em \emph{Python 3.5} utilizando os seguintes pacotes:

\begin{itemize}
\item \emph{tkinter}
\item \emph{numpy}
\item \emph{sys}
\item \emph{random}
\item \emph{math}
\end{itemize}

Os pacotes \emph{sys}, \emph{random}, \emph{tkinter} e \emph{math} fazem parte da \emph{standart library} do \emph{Python 3.5} e portanto já vem instalados, o pacote \emph{numpy} pode ser instalado, utilizando o \emph{pip}, com o seguinte comando:

\begin{figure}[H]
	\centering
	\emph{\$ python3.5 -m pip install --user numpy}.
\end{figure}


Para executar a aplicação basta rodar o seguinte comando: 
\begin{figure}[H]
	\centering
	\emph{\$ python3 main.py N}.
\end{figure}

Onde N é o número de splines aleatórias que serão geradas. Se N não for especificado o valor \emph{default} é 3.

\section{Funcionalides}
O \emph{app} abre em uma tela vazia, com um menu onde o usuário pode escolher a funcionalide que deseja utilizar.

\begin{figure}[h]
	\centering
	\includegraphics[width=.4\textwidth]{menu}
	\caption{Menu com as opções para o usuário}
\end{figure}

As funcionalides desenvolvidas são descritas a seguir.

\subsection{Qual \emph{spline} está mais próxima ?}
Ao selecionar esta opção um número N (selecionado pelo usuário via linha de comando) de \emph{splines} serão desenhadas na tela. Ao \emph{clicar} em algum ponto da tela a \emph{spline} mais próxima a este ponto será pintada de {\color{green}verde}. Como podemos ver nas Figuras \ref{fig:distance_before} e \ref{fig:distance_after}.

\begin{figure}[h]
	\centering
	\begin{minipage}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{distance_before}
		\caption{\emph{Splines} aleatórias geradas}
		\label{fig:distance_before}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{distance_after}
		\caption{\emph{Spline} muda de cor}
		\label{fig:distance_after}
	\end{minipage}
\end{figure}


\subsubsection{Equações da distância}
Para calcular a distância entre uma \emph{spline} e um ponto foi usada a função:

\begin{equation*}
f(t) = d((x_c, y_c), (x(t), y(t)))^2
\end{equation*}

Onde $(x_c, y_c)$ é o ponto e $(x(t), y(t))$ são os pontos da \emph{spline}. Para encontrar o mínimo desta função de maneira eficiente, utilizamos $f'(t) = 0$ para achar os pontos críticos. Aquele que tiver o menor valor em $f$ é o ponto de menor distância.

\begin{equation*}
f'(t) = 2(x(t) - x_c)x'(t)) + 2(y(t) - y_c)y'(t))
\end{equation*}

Para curvas de \emph{Beziér} de terceiro grau com pontos de controle $(a,b,c,d)$. Temos:

\begin{equation*}
x(t) = a_x(1 - t)^3 + 3b_x(1-t)^2t + 3c_x(1 - t)t^2 + d_xt^3
\end{equation*}

\begin{equation*}
y(t) = a_y(1 - t)^3 + 3b_y(1-t)^2t + 3c_y(1 - t)t^2 + d_yt^3
\end{equation*}

Reescrevendo as funções para facilitar os cálculos:

\begin{equation*}
x(t) = (-a_x + 3b_x - 3c_x + d_x)t^3 + (3a_x - 6b_x + 3c_x)t^2 + (-3a_x + 3b_x)t + a_x
\end{equation*}

\begin{equation*}
y(t) = (-a_y + 3b_y - 3c_y + d_y)t^3 + (3a_y - 6b_y + 3c_y)t^2 + (-3a_y + 3b_y)t + a_y
\end{equation*}

Seja $k = (-a + 3b - 3c + d)$ temos $k_x$ e $k_y$

Seja $l = (3a - 6b + 3c)$ temos $l_x$ e $l_y$

Seja $m = (-3a + 3b)$ temos $m_x$ e $m_y$

Temos então para $x$:

\begin{equation*}
x(t) = k_xt^3 + l_xt^2 + m_xt + a_x
\end{equation*}

\begin{equation*}
x'(t) = 3k_xt^2 + 2l_xt + m_x
\end{equation*}

E para $y$:

\begin{equation*}
y(t) = k_yt^3 + l_yt^2 + m_yt + a_y
\end{equation*}

\begin{equation*}
y'(t) = 3k_yt^2 + 2l_yt + m_y
\end{equation*}

Assim a função $f'$ pode ser reescrita da seguinte forma:

\begin{equation*}
f_x'(t) = 2(k_xt^3 + l_xt^2 + m_xt + a_x - x_c)(3k_xt^2 + 2l_xt + m_x)
\end{equation*}

\begin{equation*}
f_y'(t) = 2(k_yt^3 + l_yt^2 + m_yt + a_y - y_c)(3k_yt^2 + 2l_yt + m_y)
\end{equation*}

Fazendo as manipulações algebricas necessárias, chegamos em:

\begin{equation*}
\begin{split}
f_x'(t) = 2((3k_x^2)t^5  \\
+ (5k_xl_x)t^4 \\
+ (3k_xm_x + 2l_x^2 + m_xk_x)t^3 \\
+ (3k_xa_x - 3k_xx_c + 2l_xm_x + m_xl_x)t^2\\
+ (2l_xa_x - 2l_xx_c + m_x^2)t \\
+ (m_xa_x - m_xx_c))
\end{split}
\end{equation*}

\begin{equation*}
\begin{split}
f_y'(t) = 2((3k_y^2)t^5 \\
+ (5k_yl_y)t^4 \\
+ (3k_ym_y + 2l_y^2 + m_yk_y)t^3 \\
+ (3k_ya_y - 3k_yy_c + 2l_ym_y + m_yl_y)t^2 \\
+ (2l_ya_y - 2l_yy_c + m_y^2)t \\
+ (m_ya_y - m_yy_c)) \\
\end{split}
\end{equation*}

Utilizando \emph{numpy} é fácil encontrar as raizes deste polinômio e verificar qual delas é o ponto de mínimo.\footnote{O mesmo processo foi feito para curvas de \emph{Beziér} de graus 1 e 2}

\subsection{Desenhe sua \emph{spline}}
Nesta opção o usuário pode desenhar \emph{splines}. As \emph{splines} são desenhadas utilizando a algoritmo de \emph{De Casteljau}. 

Cada \emph{click} é um ponto de controle da \emph{spline}, assim temos:

\begin{itemize}
\item No primeiro \emph{click} o ponto é desenhado na tela.
\item No segundo \emph{click} a \emph{spline} de grau um (reta) é desenhada na tela.
\item No terceiro \emph{click} a \emph{spline} de grau dois é desenhada na tela.
\item No quarto \emph{click} a \emph{spline} de grau três é desenhada na tela.
\item No quinto \emph{click} o processo reinicia para uma nova \emph{spline}.
\end{itemize}

As figuras \ref{fig:draw_1}, \ref{fig:draw_2}, \ref{fig:draw_3}, \ref{fig:draw_4}, e \ref{fig:draw_5}, ilustram essa funcionalidade.

\begin{figure}[ht]
	\centering
	\begin{minipage}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{draw_1}
		\caption{Primeiro \emph{click}, ponto}
		\label{fig:draw_1}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{draw_2}
		\caption{Segundo \emph{click}, reta}
		\label{fig:draw_2}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{draw_3}
		\caption{Terceiro \emph{click}, \emph{spline} segundo grau}
		\label{fig:draw_3}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{draw_4}
		\caption{Quarto \emph{click}, \emph{spline} de terceiro grau}
		\label{fig:draw_4}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{draw_5}
		\caption{Quinto \emph{click}, inicia uma nova \emph{spline}}
		\label{fig:draw_5}
	\end{minipage}
\end{figure}

\subsection{\emph{Spline} dentro do retângulo ?}
Ao selecionar esta opção um número N (selecionado pelo usuário via linha de comando) de \emph{splines} serão desenhadas na tela. Um retângulo será desenhado a partir de dois \emph{clicks} na tela, as \emph{splines} inteiramente contidas neste retângulo serão pintadas de {\color{blue}azul}. As Figuras \ref{fig:inside_rect_1}, \ref{fig:inside_rect_2}, e \ref{fig:inside_rect_3} ilustram essa funcionalidade.

\begin{figure}[ht]
	\centering
	\begin{minipage}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{inside_rect_1}
		\caption{\emph{Splines} aleatórias geradas}
		\label{fig:inside_rect_1}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{inside_rect_2}
		\caption{Primeiro ponto do retângulo}
		\label{fig:inside_rect_2}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{inside_rect_3}
		\caption{Segundo ponto do retângulo}
		\label{fig:inside_rect_3}
	\end{minipage}
\end{figure}

\begin{figure}[H]
	\centering
	\textbf{IMPORTANTE:} Todos os retângulos devem ser desenhados na ordem mostrada nas Figuras \ref{fig:inside_rect_2} e \ref{fig:inside_rect_3}, o primeiro ponto sendo o canto superior esquerdo e o segundo ponto sendo o canto inferior direito.
\end{figure}

\subsubsection{Equações}

Para verificar se a \emph{spline} está dentro retângulo definido por:

\begin{equation*}
R = \{(x, y): x_a < x < x_b , y_a < y < y_b\}
\end{equation*}

encontramos os pontos de máximo e mínimo da \emph{spline} e verificamos se eles estão dentro do retângulo.

Seja a \emph{spline} cúbica:

\begin{equation*}
x(t) = (-a_x + 3b_x - 3c_x + d_x)t^3 + (3a_x - 6b_x + 3c_x)t^2 + (-3a_x + 3b_x)t + a_x
\end{equation*}

\begin{equation*}
y(t) = (-a_y + 3b_y - 3c_y + d_y)t^3 + (3a_y - 6b_y + 3c_y)t^2 + (-3a_y + 3b_y)t + a_y
\end{equation*}

Calculamos $x'(t) = 0$ e $y'(t) = 0$, utilizando \emph{numpy}, encontrando os pontos críticos da função:

\begin{equation*}
x'(t) = 3(-a_x + 3b_x - 3c_x + d_x)t^2 + 2(3a_x - 6b_x + 3c_x)t + (-3a_x + 3b_x) = 0
\end{equation*}

\begin{equation*}
y'(t) = 3(-a_y + 3b_y - 3c_y + d_y)t^2 + 2(3a_y - 6b_y + 3c_y)t + (-3a_y + 3b_y) = 0
\end{equation*}

Com os pontos críticos encontrados verificamos seus valores em $x(t)$ e $y(t)$, caso estes valores estejam fora de retângulo $R$ isso implica que a \emph{spline} não está totalmente contida nele, caso contrário a \emph{spline} está dentro do retângulo.

\subsection{\emph{Spline} intercepta retângulo ?}
Ao selecionar esta opção um número N (selecionado pelo usuário via linha de comando) de \emph{splines} serão desenhadas na tela. Um retângulo será desenhado a partir de dois \emph{clicks} na tela, as \emph{splines} que interceptam este retângulo serão pintadas de {\color{blue}azul}, se a \emph{spline} estiver inteiramente contida no retângulo sem tocá-lo não será pintada. As Figuras \ref{fig:intercept_1}, \ref{fig:intercept_2}, e \ref{fig:intercept_3} ilustram essa funcionalidade.

\begin{figure}[ht]
	\centering
	\begin{minipage}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{intercept_1}
		\caption{\emph{Splines} aleatórias geradas}
		\label{fig:intercept_1}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{intercept_2}
		\caption{Primeiro ponto do retângulo}
		\label{fig:intercept_2}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{intercept_3}
		\caption{Segundo ponto do retângulo}
		\label{fig:intercept_3}
	\end{minipage}
\end{figure}

\subsubsection{Equações}

Para verificar se uma \emph{spline} intercepta um retângulo definido por:

\begin{equation*}
R = \{(x, y): x_a < x < x_b , y_a < y < y_b\}
\end{equation*}

Utilizamos as equações: $f_x' = x_a$, $f_x' = x_b$, $f_y' = y_a$ e $f_y' = y_b$
Com as raizes dessas equações verificamos se:
 \begin{itemize}
 	\item A raiz está no intervalo $[0, 1]$
 	\item o ponto da \emph{spline} em $t$ respeita as outras condições do retângulo. Por exemplo se $f_x' = x_a$ tem rais em $[0, 1]$ verificamos se vale que $y_a < y* < y_b$ onde $y*$ é o valor de $y(t)$ para a raiz encontrada.
 \end{itemize} 

Satisfeitas as condições a \emph{spline} intercepta o retângulo.
\end{document}
