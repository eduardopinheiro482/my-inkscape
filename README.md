# My Inkscape

## EP1 de MAC0210:

* Baixe o inkscape em https://inkscape.org e aprenda como as ferramentas básicas
de edição de curvas de Bézier funcionam. Procure desenhos (arquivo .svg)
bacana na web para ver o que é possível fazer com tais curvas.

* Descubra como fazer um aplicativo simplezinho, na linguagem/framework que
você preferir, no qual você consegue desenhar linhas retas em uma janela e tratar
eventos de mouse. Aprenda como lidar com tais eventos.

* Usando os primitivos para traçar linhas retas, e o algoritmo de De Casteljau,
desenhe algumas curvas de Beziér de grau dois e três na tela do seu aplicativo.

* O seu aplicativo deve permitir que o usuário escolha, dentre as curvas desenhadas,
aquela que estiver mais próxima de onde ele clicar. As curvas devem ser
pintadas em vermelho, e quando o usuário clicar na tela a curva mais próxima
deverá aparecer em verde.