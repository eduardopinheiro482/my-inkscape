from tkinter import *
import Point
import ControlPoints
import math
import numpy as np
import random
import sys
import Rectangle


# global
canvas_width = 600
canvas_height = 600
canvas = None
root = None
num_splines = 3

splines = []
points_counter = 0
points_rect = []
points = []

def donothing(eventorigin):
   x = 0

def clean_up():
    global canvas, root, splines, points, points_rect, points_counter 
    canvas.delete("all")
    root.bind("<Button 1>", donothing)
    splines = []
    points_counter = 0
    points = []
    points_rect = []

def intersectRect(eventorigin):
    global points_counter, points

    canvas.delete("all")
    ClickedPoint = Point.Point(eventorigin.x, eventorigin.y)
    points_counter += 1
    points.append(ClickedPoint)
    canvas.create_oval(ClickedPoint.x, ClickedPoint.y, ClickedPoint.x, ClickedPoint.y, width=5, fill="#0000CC")

    if (points_counter == 2):
        a = Point.Point(points[0].x, points[0].y)
        b = Point.Point(points[0].x, points[1].y)
        c = Point.Point(points[1].x, points[1].y)
        d = Point.Point(points[1].x, points[0].y)
        rect = Rectangle.Rectangle(a, b, c, d)
        rect.drawRect(canvas)
        points_counter = 0
        points = []

        for s in splines:
            if rect.intersectRect(s) == True:
                s.color = "#0000FF"
            else:
                s.color = "#FF0000"

    for s in splines:
        s.printSpline(canvas)

def insideRect(eventorigin):
    global points_counter, points

    canvas.delete("all")
    ClickedPoint = Point.Point(eventorigin.x, eventorigin.y)
    points_counter += 1
    points.append(ClickedPoint)
    canvas.create_oval(ClickedPoint.x, ClickedPoint.y, ClickedPoint.x, ClickedPoint.y, width=5, fill="#0000CC")

    if (points_counter == 2):
        a = Point.Point(points[0].x, points[0].y)
        b = Point.Point(points[0].x, points[1].y)
        c = Point.Point(points[1].x, points[1].y)
        d = Point.Point(points[1].x, points[0].y)
        rect = Rectangle.Rectangle(a, b, c, d)
        rect.drawRect(canvas)
        points_counter = 0
        points = []

        for s in splines:
            if rect.insideRect(s) == True:
                s.color = "#0000FF"
            else:
                s.color = "#FF0000"

    for s in splines:
        s.printSpline(canvas)

def drawSpline(eventorigin):
    global canvas, root, points, points_counter
    canvas.delete("all")

    ClickedPoint = Point.Point(eventorigin.x, eventorigin.y)
    points.append(ClickedPoint)
    points_counter += 1

    if (points_counter == 1):
        canvas.create_oval(ClickedPoint.x, ClickedPoint.y, ClickedPoint.x, ClickedPoint.y, width=5, fill="#0000CC")
    elif (points_counter == 2):
        canvas.create_oval(points[0].x, points[0].y, points[0].x, points[0].y, width=5, fill="#0000CC")
        canvas.create_oval(points[1].x, points[1].y, points[1].x, points[1].y, width=5, fill="#0000CC")
        ControlPoints.ControlPoints(points[0], points[1], None, None, "#FF0000").printSpline(canvas)
    elif (points_counter == 3):
        canvas.create_oval(points[0].x, points[0].y, points[0].x, points[0].y, width=5, fill="#0000CC")
        canvas.create_oval(points[1].x, points[1].y, points[1].x, points[1].y, width=5, fill="#0000CC")
        canvas.create_oval(points[2].x, points[2].y, points[2].x, points[2].y, width=5, fill="#0000CC")
        ControlPoints.ControlPoints(points[0], points[1], points[2], None, "#FF0000").printSpline(canvas)
    else:
        canvas.create_oval(points[0].x, points[0].y, points[0].x, points[0].y, width=5, fill="#0000CC")
        canvas.create_oval(points[1].x, points[1].y, points[1].x, points[1].y, width=5, fill="#0000CC")
        canvas.create_oval(points[2].x, points[2].y, points[2].x, points[2].y, width=5, fill="#0000CC")
        canvas.create_oval(points[3].x, points[3].y, points[3].x, points[3].y, width=5, fill="#0000CC")
        spline = ControlPoints.ControlPoints(points[0], points[1], points[2], points[3], "#FF0000")
        spline.printSpline(canvas)
        splines.append(spline)
        points = []
        points_counter = 0

    for s in splines:
        s.printSpline(canvas)


def distanceCalc(eventorigin):
    global x_click,y_click, points_counter, points_rect, splines, canvas, root

    canvas.delete("all")

    ClickedPoint = Point.Point(eventorigin.x, eventorigin.y)

    canvas.create_oval(ClickedPoint.x, ClickedPoint.y, ClickedPoint.x, ClickedPoint.y, width=5, fill="#0000CC")

    min_dist = sys.maxsize
    t_min = 0
    spline_min = 0
    roots = []

    for spline in splines:
        roots.append(spline.roots(ClickedPoint))

    for i in range(len(roots)):
        for r in roots[i]:
            r = abs(r)
            if (r >= 0.0 and r <= 1.0):
                distance_points = splines[i].bezier(r)
                distance = distance_points.QuadraticDistance(ClickedPoint) 
                if (distance < min_dist):
                    t_min = r
                    min_dist = distance
                    spline_min = i

    for i in range(len(splines)):
        if i == spline_min:
            splines[i].color = "#00FF00"
        else:
            splines[i].color = "#FF0000" 

    for spline in splines:
        spline.printSpline(canvas)

def random_spline(type):
    global splines, canvas_width, canvas_height, canvas, num_splines

    for i in range(num_splines):
        if type == "distance":
            n = random.randint(3, 4) # splines de segundo e terceiro grau para distancia
        else:
            n = 4 # splines de terceiro grau para retangulos
        coorArrX = [None, None, None, None]
        coorArrY = [None, None, None, None]
        for k in range(n):
            x = random.randint(0, canvas_width - 1)
            y = random.randint(0, canvas_height - 1)
            coorArrX[k] = x
            coorArrY[k] = y
        if (coorArrX[2] == None and coorArrX[3] == None): 
            splines.append(ControlPoints.ControlPoints(Point.Point(coorArrX[0], coorArrY[0]), 
                Point.Point(coorArrX[1], coorArrY[1]), None, None, "#FF0000"))
        elif (coorArrX[3] == None):
            splines.append(ControlPoints.ControlPoints(Point.Point(coorArrX[0], coorArrY[0]), 
            Point.Point(coorArrX[1], coorArrY[1]), Point.Point(coorArrX[2], coorArrY[2]), None, "#FF0000"))
        else:
            splines.append(ControlPoints.ControlPoints(Point.Point(coorArrX[0], coorArrY[0]), 
            Point.Point(coorArrX[1], coorArrY[1]), Point.Point(coorArrX[2], coorArrY[2]), Point.Point(coorArrX[3], coorArrY[3]),
            "#FF0000"))

    for spline in splines:
        spline.printSpline(canvas)

def intersect_rect_bind():
    global root

    clean_up()
    random_spline("intercept")
    root.bind("<Button 1>", intersectRect)

def inside_rect_bind():
    global root

    clean_up()
    random_spline("inside")
    root.bind("<Button 1>", insideRect)

def distance_bind():
    global root

    clean_up()
    random_spline("distance")
    root.bind("<Button 1>", distanceCalc)

def draw_spline_bind():
    global root

    clean_up()
    root.bind("<Button 1>", drawSpline)

def main():
    global canvas_width, canvas_height, canvas, root, num_splines

    if len(sys.argv) >= 2:
        num_splines = int(sys.argv[1])

    # tk init
    root = Tk()
    root.title("My Inkscape")

    # Canvas
    canvas = Canvas(root, 
           width=canvas_width,
           height=canvas_height,
           bg="#FFFFFF")
    canvas.pack()

    # Put Menu
    menubar = Menu(root)
    filemenu = Menu(menubar, tearoff=0)
    filemenu.add_command(label="Qual spline está mais próxima do ponto ?", command=distance_bind)
    filemenu.add_command(label="Desenhe sua spline", command=draw_spline_bind)
    filemenu.add_command(label="Spline dentro do retângulo ?", command=inside_rect_bind)
    filemenu.add_command(label="Spline intercepta retângulo ?", command=intersect_rect_bind)
    filemenu.add_command(label="Limpar tela", command=clean_up)
    filemenu.add_separator()
    filemenu.add_command(label="Sair", command=root.quit)
    menubar.add_cascade(label="Opções", menu=filemenu)
     
    root.config(menu=menubar)
    root.mainloop()

main()

def verifyDistance(ClickedPoint):
    t = 0.0
    min_dist = sys.maxsize
    t_min = 0
    while (t < 1.0):
        t += 0.001
        distance_points = spline1.bezier(t)
        distance = distance_points.QuadraticDistance(ClickedPoint)
        if (distance < min_dist):
            t_min = t
            min_dist = distance
    print(t_min, min_dist)
