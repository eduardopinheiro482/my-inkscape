from numpy.polynomial import Polynomial as P
import numpy as np
import Point
import math

class ControlPoints:
    def __init__(self, a, b, c, d, color):
        self.a = a
        self.b = b
        self.c = c
        self.d = d
        self.color = color

    # Call for root calculation of any kind
    def roots(self, point):
        if (self.c == None and self.d == None): 
            return self.__roots1(point)
        elif (self.d == None):
            return self.__roots2(point)
        else:
            return self.__roots3(point)

    def printSpline(self, canvas):
        if (self.c == None and self.d == None):
            self.__deCasteljau1(canvas)
        elif (self.d == None):
            self.__deCasteljau2(canvas)
        else:
            self.__deCasteljau3(canvas)

    def bezier(self, t):
        if (self.c == None and self.d == None): 
            return self.__bezier1(t)
        elif (self.d == None):
            return self.__bezier2(t)
        else:
            return self.__bezier3(t)

    def minMaxX(self):
        return self.__minMaxX3()

    def minMaxY(self):
        return self.__minMaxY3()

    def intersectX(self, d):
        return self.__intersectX3(d)

    def intersectY(self, d):
        return self.__intersectY3(d)

    def intersectLine(self, a, b):
        return self.__intersecLine2(a, b)

#private methods
    def __deCasteljau1(self, canvas):
        canvas.create_line(self.a.x, self.a.y, self.b.x, self.b.y, fill = self.color)

    def __deCasteljau2(self, canvas):
        coorArrX = [self.a.x, self.b.x, self.c.x]
        coorArrY = [self.a.y, self.b.y, self.c.y]

        qX = []
        qY = []

        for i in range(len(coorArrX) - 1):
            qX.append((coorArrX[i] + coorArrX[i+1]) / 2)

        for i in range(len(coorArrX) - 1):
            qY.append((coorArrY[i] + coorArrY[i+1]) / 2)

        pX = (qX[0] + qX[1]) / 2
        pY = (qY[0] + qY[1]) / 2

        spline1 = ControlPoints(Point.Point(coorArrX[0], coorArrY[0]), Point.Point(qX[0], qY[0]),
            Point.Point(pX, pY), None, self.color)

        spline2 = ControlPoints(Point.Point(pX, pY), Point.Point(qX[1], qY[1]), Point.Point(coorArrX[2], coorArrY[2]),
            None, self.color)

        distance1 = ControlPoints.__twoPointLineDistance(Point.Point(coorArrX[0], coorArrY[0]), Point.Point(pX, pY), Point.Point(qX[0], qY[0]))
        distance2 = ControlPoints.__twoPointLineDistance(Point.Point(pX, pY), Point.Point(coorArrX[2], coorArrY[2]), Point.Point(qX[1], qY[1]))

        if (distance1 <= 1.0):
            canvas.create_line(round(coorArrX[0], 0), round(coorArrY[0], 0), round(pX, 0), round(pY, 0), fill = self.color)
        else:
            spline1.__deCasteljau2(canvas)

        if (distance2 <= 1.0):
            canvas.create_line(round(pX, 0), round(pY, 0), round(coorArrX[2], 0), round(coorArrY[2], 0), fill = self.color)
        else:
            spline2.__deCasteljau2(canvas)
        return


    def __deCasteljau3(self, canvas):
        coorArrX = [self.a.x, self.b.x, self.c.x, self.d.x]
        coorArrY = [self.a.y, self.b.y, self.c.y, self.d.y]

        qX = []
        qY = []

        rX = []
        rY = []
        for i in range(len(coorArrX) - 1):
            qX.append((coorArrX[i] + coorArrX[i+1]) / 2)

        for i in range(len(coorArrX) - 1):
            qY.append((coorArrY[i] + coorArrY[i+1]) / 2)

        for i in range(len(qX) - 1):
            rX.append((qX[i] + qX[i+1]) / 2)

        for i in range(len(qY) - 1):
            rY.append((qY[i] + qY[i+1]) / 2)

        pX = (rX[0] + rX[1]) / 2
        pY = (rY[0] + rY[1]) / 2

        spline1 = ControlPoints(Point.Point(coorArrX[0], coorArrY[0]), Point.Point(qX[0], qY[0]),
            Point.Point(rX[0], rY[0]), Point.Point(pX, pY), self.color)

        spline2 = ControlPoints(Point.Point(pX, pY), Point.Point(rX[1], rY[1]), Point.Point(qX[2], qY[2]),
            Point.Point(coorArrX[3], coorArrY[3]), self.color)

        distance1 = ControlPoints.__twoPointLineDistance(Point.Point(coorArrX[0], coorArrY[0]), Point.Point(pX, pY), Point.Point(qX[0], qY[0]))
        distance1b = ControlPoints.__twoPointLineDistance(Point.Point(coorArrX[0], coorArrY[0]), Point.Point(pX, pY), Point.Point(rX[0], rY[0]))
        distance2 = ControlPoints.__twoPointLineDistance(Point.Point(pX, pY), Point.Point(coorArrX[3], coorArrY[3]), Point.Point(rX[1], rY[1]))
        distance2b = ControlPoints.__twoPointLineDistance(Point.Point(pX, pY), Point.Point(coorArrX[3], coorArrY[3]), Point.Point(qX[2], qY[2]))

        if (distance1 <= 1.0 and distance1b <= 1.0):
            canvas.create_line(round(coorArrX[0], 0), round(coorArrY[0], 0), round(pX, 0), round(pY, 0), fill = self.color)
        else:
            spline1.__deCasteljau3(canvas)

        if (distance2 <= 1.0 and distance2b <= 1.0):
            canvas.create_line(round(pX, 0), round(pY, 0), round(coorArrX[3], 0), round(coorArrY[3], 0), fill = self.color)
        else:
            spline2.__deCasteljau3(canvas)
        return

    @classmethod
    def __twoPointLineDistance(cls, pointLine1, pointLine2, point):
        k = abs ( ((pointLine2.y - pointLine1.y) * point.x) - ((pointLine2.x - pointLine1.x) * point.y) + 
            (pointLine2.x * pointLine1.y) - (pointLine2.y * pointLine1.x) )

        l = math.sqrt((pointLine2.y - pointLine1.y)**2 + (pointLine2.x - pointLine1.x)**2)

        return k/l

    def __intersecLine2(self, a, b):
        polynomial = [self.a.y + (-a * self.a.x) - b, #t^0
                      (-2 * self.a.y) + (2 * self.b.y) + (2 * a * self.a.x) + (-2 * a * self.b.x), # t^1
                      (self.a.y) + (-2 * self.b.y) + (self.c.y) + (-a * self.a.x) + (2 * a * self.b.x) + (-a * self.c.x) #tˆ2 
        ]
        poly = P(polynomial)
        return poly.roots()


    def __bezier1(self, t):
        x = self.a.x * (1-t) + self.b.x * t
        y = self.a.y * (1-t) + self.b.y * t
        return Point.Point(x, y)


    def __bezier2(self, t):
        x = self.a.x * (1- t)**2 + 2 * self.b.x * (1 - t)*t + self.c.x * t**2
        y = self.a.y * (1- t)**2 + 2 * self.b.y * (1 - t)*t + self.c.y * t**2
        return Point.Point(x, y)

    def __bezier3(self, t):
        x = (self.a.x * ((1-t)**3)) + (3 * self.b.x * (((1-t)**2) * t)) + 3 * (self.c.x * ((1-t) * t**2)) + (self.d.x * (t**3))
        y = (self.a.y * ((1-t)**3)) + (3 * self.b.y * (((1-t)**2) * t)) + 3 * (self.c.y * ((1-t) * t**2)) + (self.d.y * (t**3))
        return Point.Point(x, y)

    def __intersectX3(self, d):
        polynomial = [(self.a.x - d), # t^0
                      (-3 * self.a.x) + (3 * self.b.x), #t^1
                      (3 * self.a.x) + (-6 * self.b.x) + (3 * self.c.x), # t^2
                      (- self.a.x) + (3 * self.b.x) + (-3 * self.c.x) + (self.d.x) #tˆ3
        ]
        poly = P(polynomial)
        return poly.roots()

    def __intersectY3(self, d):
        polynomial = [(self.a.y - d), # t^0
                      (-3 * self.a.y) + (3 * self.b.y), #t^1
                      (3 * self.a.y) + (-6 * self.b.y) + (3 * self.c.y), # t^2
                      (- self.a.y) + (3 * self.b.y) + (-3 * self.c.y) + (self.d.y) #tˆ3
        ]
        poly = P(polynomial)
        return poly.roots()

    def __minMaxX3(self):
        f_line = [(-3 * self.a.x) + (3 * self.b.x), # t^0
                  (6 * self.a.x) + (- 12 * self.b.x) + (6 * self.c.x), # t^1
                  (-3 * self.a.x) + (9 * self.b.x) + (-9 * self.c.x) + (3 * self.d.x) # t^2
        ]
        poly = P(f_line)
        roots = poly.roots()
        roots = np.append(roots, 0.0)
        roots = np.append(roots, 1.0)
        return roots

    def __minMaxY3(self):
        f_line = [(-3 * self.a.y) + (3 * self.b.y), # t^0
                  (6 * self.a.y) + (- 12 * self.b.y) + (6 * self.c.y), # t^1
                  (-3 * self.a.y) + (9 * self.b.y) + (-9 * self.c.y) + (3 * self.d.y) # t^2
        ]
        poly = P(f_line)
        roots = poly.roots()
        roots = np.append(roots, 0.0)
        roots = np.append(roots, 1.0)
        return roots

    # Distance for linear bezier splines
    def __roots1(self, point):
        f_line = [-(self.a.x ** 2) + (self.b.x * self.a.x) + (self.a.x * point.x) - (self.b.x * point.x) 
                  -(self.a.y ** 2) + (self.b.y * self.a.y) + (self.a.y * point.y) - (self.b.y * point.y),
                  (((self.a.x ** 2)) - (2 * self.a.x * self.b.x) + (self.b.x**2)) + 
                  (((self.a.y ** 2)) - (2 * self.a.y * self.b.y) + (self.b.y**2))
        ]
        poly = P(f_line)
        roots = poly.roots()
        roots = np.append(roots, 0.0)
        roots = np.append(roots, 1.0)
        return roots

    # Distance for quadratic bezier splines
    def __roots2(self, point):
        kx = (self.a.x - 2.0 * self.b.x + self.c.x)
        jx = (-2.0 * self.a.x + 2.0 * self.b.x)

        ky = (self.a.y - 2.0 * self.b.y + self.c.y)
        jy = (-2.0 * self.a.y + 2.0 * self.b.y)

        f_line = [(self.a.x * jx) - (jx * point.x)
                + (self.a.y * jy) - (jy * point.y), # t^0
                  (self.a.x * (2*kx)) - (2 * (kx * point.x)) + (jx**2) +
                  (self.a.y * (2*ky)) - (2 * (ky * point.y)) + (jy**2), #t^1
                  (3 * jx*kx)
                + (3 * jy*ky), # t^2
                  (2 * (kx ** 2)) 
                + (2 * (ky ** 2))  # t^3
        ]
        poly = P(f_line)
        roots = poly.roots()
        roots = np.append(roots, 0.0)
        roots = np.append(roots, 1.0)
        return roots

    # Distance for cubic bezier splines
    def __roots3(self, point):
        kx = (-self.a.x + 3 * self.b.x - 3 * self.c.x + self.d.x)
        lx = (3 * self.a.x - 6 * self.b.x + 3 * self.c.x)
        mx = (-3 * self.a.x + 3 * self.b.x)

        ky = (-self.a.y + 3 * self.b.y - 3 * self.c.y + self.d.y)
        ly = (3 * self.a.y - 6 * self.b.y + 3 * self.c.y)
        my = (-3 * self.a.y + 3 * self.b.y)

        f_line = [ (mx * self.a.x) - (mx * point.x)
                 + (my * self.a.y) - (my * point.y), # t^0
                   (2 * lx * self.a.x) - (2 * lx * point.x) + (mx ** 2)
                 + (2 * ly * self.a.y) - (2 * ly * point.y) + (my ** 2), # t^1
                   (3 * kx * self.a.x) - (3 * kx * point.x) + (2 * lx * mx) + (mx * lx)
                 + (3 * ky * self.a.y) - (3 * ky * point.y) + (2 * ly * my) + (my * ly), # t^2
                   (3 * kx * mx) + (2 * lx**2) + (mx * kx)
                 + (3 * ky * my) + (2 * ly**2) + (my * ky), # t^3
                   (5 * kx * lx)
                 + (5 * ky * ly), # t^4
                   (3 * kx**2)
                 + (3 * ky**2) # t^5
                ]
        poly = P(f_line)
        roots = poly.roots()
        roots = np.append(roots, 0.0)
        roots = np.append(roots, 1.0)
        return roots
