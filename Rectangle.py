import ControlPoints
import numpy as np

class Rectangle:
    def __init__(self, a, b, c, d):
        self.a = a
        self.b = b
        self.c = c
        self.d = d

    # def __init__(self, points):
    #     self.a = points[0]
    #     self.b = points[1]
    #     self.c = points[2]
    #     self.d = points[3]

    def drawRect(self, canvas):
        canvas.create_line(self.a.x, self.a.y, self.b.x, self.b.y)
        canvas.create_line(self.b.x, self.b.y, self.c.x, self.c.y)
        canvas.create_line(self.c.x, self.c.y, self.d.x, self.d.y)
        canvas.create_line(self.d.x, self.d.y, self.a.x, self.a.y)

    def insideRect(self, spline):
        minmaxX = spline.minMaxX()
        minmaxY = spline.minMaxY()
        
        for m in minmaxX:
            m = abs(m)
            if m >= 0.0 and m <= 1.0:
                val = spline.bezier(m)
                if val.x < self.a.x or val.x > self.c.x:
                    return False

        for m in minmaxY:
            m = abs(m)
            if m >= 0.0 and m <= 1.0:
                val = spline.bezier(m)
                if val.y < self.a.y or val.y > self.b.y:
                    return False

        return True

    def intersectRect(self, spline):
        interXmin = spline.intersectX(self.a.x)
        interYmin = spline.intersectY(self.a.y)

        interXmax = spline.intersectX(self.d.x)
        interYmax = spline.intersectY(self.b.y)

        interXmin = interXmin[np.isreal(interXmin)]
        interYmin = interYmin[np.isreal(interYmin)]
        interXmax = interXmax[np.isreal(interXmax)]
        interYmax = interYmax[np.isreal(interYmax)]

        for i in interXmin:
            if i >= 0.0 and i <= 1.0:
                val = spline.bezier(i)
                if val.y <= self.b.y and val.y >= self.a.y:
                    return True

        for i in interYmin:
            if i >= 0.0 and i <= 1.0:
                val = spline.bezier(i)
                if val.x <= self.d.x and val.x >= self.a.x:
                    return True

        for i in interXmax:
            if i >= 0.0 and i <= 1.0:
                val = spline.bezier(i)
                if val.y <= self.b.y and val.y >= self.a.y:
                    return True
        
        for i in interYmax:
            if i >= 0.0 and i <= 1.0:
                val = spline.bezier(i)
                if val.x <= self.d.x and val.x >= self.a.x:
                    return True

        return False
